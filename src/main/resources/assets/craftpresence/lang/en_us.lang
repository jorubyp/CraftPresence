#PARSE_ESCAPES
## CONFIG GUI - TITLE NAMES ##
gui.config.title=CraftPresence - Configuration GUI
gui.config.title.general=General Settings
gui.config.title.biomemessages=Customize Biome Messages
gui.config.title.dimensionmessages=Customize Dimension Messages
gui.config.title.servermessages=Customize Server Messages
gui.config.title.statusmessages=Customize Status Messages
gui.config.title.advanced=Advanced Settings
gui.config.title.accessibility=Accessibility Settings
gui.config.title.about.config=About this Configuration GUI
gui.config.title.commands=Commands
gui.config.title.message=CraftPresence - Message
gui.config.title.biome.editspecificbiome=CraftPresence - Edit Biome (%1$s)
gui.config.title.dimension.editspecificdimension=CraftPresence - Edit Dimension (%1$s)
gui.config.title.server.editspecificserver=CraftPresence - Edit Server (%1$s)
gui.config.title.gui.editspecificgui=CraftPresence - Edit GUI (%1$s)
gui.config.title.gui.editspecificitem=CraftPresence - Edit Item (%1$s)
gui.config.title.editor.color=CraftPresence - Color Editor (%1$s)
gui.config.title.editor.character=CraftPresence - Character Editor
gui.config.title.editor.addnew=CraftPresence - Add New Value
gui.config.title.selector.dimension=CraftPresence - Select a Dimension
gui.config.title.selector.biome=CraftPresence - Select a Biome
gui.config.title.selector.server=CraftPresence - Select Server IP
gui.config.title.selector.gui=CraftPresence - Select a GUI
gui.config.title.selector.item=CraftPresence - Select an Item
gui.config.title.selector.icon=CraftPresence - Select an Icon
gui.config.title.selector.view.assets.large=CraftPresence - Discord Large Assets
gui.config.title.selector.view.assets.small=CraftPresence - Discord Small Assets
gui.config.title.selector.view.assets.all=CraftPresence - Discord Assets List
gui.config.title.selector.view.biomes=CraftPresence - Available Biomes
gui.config.title.selector.view.dimensions=CraftPresence - Available Dimensions
gui.config.title.selector.view.servers=CraftPresence - Server Addresses
gui.config.title.selector.view.guis=CraftPresence - Available GUIs
gui.config.title.selector.view.items=CraftPresence - Available Items
## CONFIG MESSAGES - TITLE COMMENTS ##
gui.config.comment.title.general=General Settings for Displaying Info
gui.config.comment.title.biomemessages=Customize Messages to display with Biomes \n (Manual Format: biome_name;message) \n Available Placeholders: \n - &biome& = Biome Name \n - &id& = Biome ID
gui.config.comment.title.dimensionmessages=Customize Messages to display with Dimensions \n (Manual Format: dimension_name;message) \n Available Placeholders: \n - &dimension& = Dimension Name \n - &id& = Dimension ID \n - &icon& = Default Dimension Icon
gui.config.comment.title.servermessages=Customize Messages to display with Servers \n (Manual Format: server_IP;message;icon) \n Available Placeholders: \n - &ip& = Server IP \n - &name& = Server Name \n - &motd& = Server MOTD \n - &icon& = Default Server Icon \n - &players& = Server Player Counter \n - &ign& = Your Minecraft Username \n - &time& = Your World Game Time \n - &mods& = The Amount of Mods currently in your Mods Folder
gui.config.comment.title.statusmessages=Customize Messages relating to different Game States
gui.config.comment.title.advanced=Customize Additional Settings of the Mod
gui.config.comment.title.accessibility= Customize Accessibility Settings of the Mod \n Includes: \n - Language ID \n - KeyBindings \n - Additional GUI Customization Options
## CONFIG GUI - BUTTONS AND NAMES ##
gui.config.buttonMessage.back=Back
gui.config.buttonMessage.save=Save
gui.config.buttonMessage.continue=Continue
gui.config.buttonMessage.viewsource=View Source
gui.config.buttonMessage.addnew=Add New
gui.config.buttonMessage.iconchange=Change Icon
gui.config.buttonMessage.chareditor=Character Editor
gui.config.buttonMessage.sync.single=Sync %1$s
gui.config.buttonMessage.sync.all=Sync All
gui.config.buttonMessage.reset=Reset To Default
gui.config.defaultMessage.biome=Default Biome Message
gui.config.defaultMessage.dimension=Default Dimension Message
gui.config.defaultMessage.server=Default Server Message
gui.config.editorMessage.charinput=Character Input:
gui.config.editorMessage.charwidth=Character Width:
gui.config.editorMessage.search=Search:
gui.config.editorMessage.message=Message:
gui.config.editorMessage.enterkey=Enter Key...
gui.config.editorMessage.valuename=Value Name:
gui.config.editorMessage.hexcode=Hex Code:
gui.config.editorMessage.texturepath=Texture Path:
gui.config.editorMessage.preview=Preview:
gui.config.editorMessage.redcolorvalue=Red Color Value:
gui.config.editorMessage.greencolorvalue=Green Color Value:
gui.config.editorMessage.bluecolorvalue=Blue Color Value:
gui.config.editorMessage.alphacolorvalue=Alpha Color Value:
gui.config.editorMessage.refresh=**Press ENTER to Refresh Values**
gui.config.message.character.notice=Notice on Syncing \n\n Syncing may not yield accurate Results on Older Minecraft Versions
gui.config.message.credits=This Configuration GUI was made from scratch by \n Jonathing, and will continue to be maintained by \n CDAGaming. A lot of effort went into making this \n custom GUI, so show him some support! Thanks. \n\n Feel free to learn from this GUI's code on \n the CraftPresence GitLab repository.
gui.config.message.remove=**Remove Message Text to Remove this Value**
gui.config.message.null=This GUI is not Implemented just yet! \n\n Please Check Back Later...
gui.config.message.emptylist=This List is Empty and Cannot be Displayed! \n\n Please Try Again...
gui.config.hoverMessage.access=**Enable "%1$s" to use this Menu**
gui.config.hoverMessage.defaultempty=**Default Info cannot be Empty and Must be Valid**
gui.config.hoverMessage.valuename=Name to Identify this Value
## CONFIG MESSAGES - NAMES ##
gui.config.name.general.clientid=Client ID
gui.config.name.general.defaulticon=Default Icon
gui.config.name.general.detectcursemanifest=Detect Curse Manifest
gui.config.name.general.detectmultimcmanifest=Detect MultiMC Instance
gui.config.name.general.detectmcupdaterinstance=Detect MCUpdater Instance
gui.config.name.general.detecttechnicpack=Detect Technic Pack
gui.config.name.general.showtime=Show Elapsed Time
gui.config.name.general.showbiome=Show Biome
gui.config.name.general.showdimension=Show Dimension
gui.config.name.general.showstate=Show Game Status
gui.config.name.general.enablejoinrequest=Enable Join Requests
gui.config.name.biomemessages.biomemessages=Biome Messages
gui.config.name.dimensionmessages.dimensionicon=Default Dimension Icon
gui.config.name.dimensionmessages.dimensionmessages=Dimension Messages
gui.config.name.servermessages.servericon=Default Server Icon
gui.config.name.servermessages.servermotd=Default Server MOTD
gui.config.name.servermessages.servername=Default Server Name
gui.config.name.servermessages.servermessages=Server Messages
gui.config.name.statusmessages.mainmenumsg=Main Menu Message
gui.config.name.statusmessages.lanmsg=LAN Game Message
gui.config.name.statusmessages.singleplayermsg=Singleplayer Game Message
gui.config.name.statusmessages.placeholder.packmsg=ModPack Message
gui.config.name.statusmessages.placeholder.playermsg=Player Name Placeholder
gui.config.name.statusmessages.placeholder.playeramountmsg=Player List Placeholder
gui.config.name.statusmessages.placeholder.gametimemsg=Game Time Placeholder
gui.config.name.statusmessages.placeholder.modsmsg=Mods Placeholder
gui.config.name.statusmessages.special.vivecraftmsg=Vivecraft Message
gui.config.name.advanced.splitcharacter=Split Character
gui.config.name.advanced.enablecommands=Enable Commands
gui.config.name.advanced.enablepergui=Enable Per-GUI System
gui.config.name.advanced.enableperitem=Enable Per-Item System
gui.config.name.advanced.overwriteservericon=Overwrite Server Icon
gui.config.name.advanced.rendertooltips=Render Tooltips
gui.config.name.advanced.guimessages=GUI Messages
gui.config.name.advanced.itemmessages=Item Messages
gui.config.name.accessibility.tooltipbgcolor=Tooltip Background Color
gui.config.name.accessibility.tooltipbordercolor=Tooltip Border Color
gui.config.name.accessibility.guibgcolor=GUI Background Color
gui.config.name.accessibility.languageid=Language ID
gui.config.name.accessibility.striptranslationcolors=Strip Translation Colors
gui.config.name.accessibility.showlogginginchat=Show Logging in Chat
## CONFIG MESSAGES - COMMENTS ##
gui.config.comment.general.clientid=Client ID Used for retrieving Assets, Icon Keys, and titles
gui.config.comment.general.defaulticon=Default Icon \n (Used in Main Menu, Dimensions and Servers)
gui.config.comment.general.detectcursemanifest=Enable Detection for Twitch/Curse Manifest Data?
gui.config.comment.general.detectmultimcmanifest=Enable Detection for MultiMC Instance Data?
gui.config.comment.general.detectmcupdaterinstance=Enable Detection for MCUpdater Instance Data?
gui.config.comment.general.detecttechnicpack=Enable Detection for Technic Pack Data?
gui.config.comment.general.showtime=Show Elapsed Time in Rich Presence?
gui.config.comment.general.showbiome=Show Current Biome in Rich Presence? \n (Will Disable Game Status)
gui.config.comment.general.showdimension=Show Current Dimension in Rich Presence?
gui.config.comment.general.showstate=Show Specific Game Data in Rich Presence? \n (Will Disable Current Biome)
gui.config.comment.general.enablejoinrequest=Allows Sending or Accepting Join Requests in Discord
gui.config.comment.biomemessages.biomemessages=Customize Messages to display with Biomes \n (Manual Format: biome_name;message) \n Available Placeholders: \n - &biome& = Biome Name \n - &id& = Biome ID
gui.config.comment.dimensionmessages.dimensionicon=Dimension Icon to Default to when in an Unsupported Dimension
gui.config.comment.dimensionmessages.dimensionmessages=Customize Messages to display with Dimensions \n (Manual Format: dimension_name;message) \n Available Placeholders: \n - &dimension& = Dimension Name \n - &id& = Dimension ID \n - &icon& = Default Dimension Icon
gui.config.comment.servermessages.servericon=Server Icon to default to when in an Unsupported Server
gui.config.comment.servermessages.servermotd=Server MOTD to default to, in the case of a null MOTD \n (And in Direct Connects)
gui.config.comment.servermessages.servername=Server Display Name to default to, in the case of a null Name \n (And in Direct Connects)
gui.config.comment.servermessages.servermessages=Customize Messages to display with Servers \n (Manual Format: server_IP;message;icon) \n Available Placeholders: \n - &ip& = Server IP \n - &name& = Server Name \n - &motd& = Server MOTD \n - &icon& = Default Server Icon \n - &players& = Server Player Counter \n - &ign& = Your Minecraft Username \n - &time& = Your World Game Time \n - &mods& = The Amount of Mods currently in your Mods Folder
gui.config.comment.statusmessages.mainmenumsg=Message to Display while on the Main Menu \n Available Placeholders: \n - &ign& = Your Minecraft Username \n - &mods& = The Amount of Mods currently in your Mods Folder
gui.config.comment.statusmessages.lanmsg=Message to Display while in a LAN Game \n Available Placeholders: \n - &ip& = Server IP \n - &name& = Server Name \n - &motd& = Server MOTD \n - &icon& = Default Server Icon \n - &players& = Server Player Counter \n - &ign& = Your Minecraft Username \n - &time& = Your World Game Time \n - &mods& = The Amount of Mods currently in your Mods Folder
gui.config.comment.statusmessages.singleplayermsg=Message to Display while in Singleplayer \n Available Placeholders: \n - &ign& = Your Minecraft Username \n - &time& = Your World Game Time \n - &mods& = The Amount of Mods currently in your Mods Folder
gui.config.comment.statusmessages.placeholder.packmsg=Message to display in Rich Presence when using a valid ModPack \n (Curse, Twitch, Technic, MCUpdater, or MultiMC) \n Available Placeholders: \n - &name& = The Pack's Name
gui.config.comment.statusmessages.placeholder.playermsg=Message to use for the &ign& Placeholder \n Available Placeholders: \n - &name& = Your Username
gui.config.comment.statusmessages.placeholder.playeramountmsg=Message to use for the &players& Placeholder \n Available Placeholders: \n - &current& = Current Player Count \n - &max& = Maximum Player Count
gui.config.comment.statusmessages.placeholder.gametimemsg=Message to use for the &time& Placeholder \n Available Placeholders: \n - &worldtime& = The Current World Time, in format of HH:MM
gui.config.comment.statusmessages.placeholder.modsmsg=Message to use for the &mods& Placeholder \n Available Placeholders: \n - &modcount& = The Amount of Mods currently in your Mods Folder
gui.config.comment.statusmessages.special.vivecraftmsg=Message to display in Rich Presence when using Vivecraft
gui.config.comment.advanced.splitcharacter=The Split Character for The Arrays in CraftPresence's Messages Configs \n (You'll need to change each of your variables manually)
gui.config.comment.advanced.enablecommands=Allows CraftPresence to utilize Commands \n (Enables Command GUI in Main Config GUI)
gui.config.comment.advanced.enablepergui=Allows CraftPresence to change it's display based on the GUI your in \n Note the Following: \n - Overwrites Biomes \n - Requires an Option in GUI Messages \n - Minecraft's GUIs must be Opened once before Configuring due to Obfuscation
gui.config.comment.advanced.enableperitem=Allows CraftPresence to change it's display based on the Item Your Holding \n Note the Following: \n - Requires an Option in Item Messages
gui.config.comment.advanced.overwriteservericon=Allows CraftPresence to overwrite the Server Icon and Text if a Pack is in use
gui.config.comment.advanced.rendertooltips=Allow CraftPresence to render Hover Tooltips (when available)
gui.config.comment.advanced.guimessages=Customize Messages to display with GUIs \n (Manual Format: gui;message) \n Available Placeholders: \n - &gui& = GUI Name \n - &class& = GUI Class Name \n - &screen& = GUI Screen Instance Name
gui.config.comment.advanced.itemmessages=Customize Messages to display with Items \n (Manual Format: item;message) \n Available Placeholders: \n - &main& = Current Main Hand Item \n - &offhand& = Current OffHand Item \n - &helmet& = Currently Equipped Helmet \n - &chest& = Currently Equipped ChestPiece \n - &legs& = Currently Equipped Leggings \n - &boots& = Currently Equipped Boots
## KEYBIND MESSAGES ##
key.craftpresence.category=CraftPresence - KeyBindings
key.craftpresence.config_keybind=Config KeyBinding
## DEFAULT MESSAGES ##
craftpresence.message.unsupported=This Feature is not Supported in this Version of Minecraft
craftpresence.defaults.servermessages.servername=Minecraft Server
craftpresence.defaults.servermessages.servermotd=A Minecraft Server
craftpresence.defaults.state.mainmenu=In the Main Menu
craftpresence.defaults.state.mcversion=Minecraft %1$s
craftpresence.defaults.state.lan=Playing on a LAN Server
craftpresence.defaults.state.singleplayer=Playing Singleplayer
craftpresence.defaults.placeholder.pack=&name&
craftpresence.defaults.placeholder.players=&current& / &max& Players
craftpresence.defaults.placeholder.ign=&name&
craftpresence.defaults.placeholder.time=&worldtime&
craftpresence.defaults.placeholder.mods=&modcount& Mods
craftpresence.defaults.special.vivecraft=Playing in VR via Vivecraft
craftpresence.multiplayer.status.cannot_connect=Can't connect to server
craftpresence.multiplayer.status.pinging=Pinging...
craftpresence.multiplayer.status.polling=Polling...
craftpresence.multiplayer.status.cannot_resolve=Can't resolve hostname
## COMMAND MESSAGES ##
craftpresence.command.usage.main=§lCraftPresence - Sub-Commands: \n §rSyntax: §6/<cp|craftpresence> <command> \n\n §6§lreboot §r- Reboot RPC \n §6§lshutdown §r- Shut Down RPC \n §6§lreload §r- Reloads CraftPresence Data based on Settings \n §6§lrequest §r- View Join Request Info \n §6§lview §r- View a Variety of Display Data \n §6§lhelp §r- Views this Page
craftpresence.command.usage.view=§lCraftPresence - View Sub-Commands: \n\n §6§lcurrentData §r- Shows your Current RPC Data, in text \n §6§lassets §r- Displays all Asset Icons available \n §6§ldimensions §r- Displays available Dimension Names \n §6§lbiomes §r- Displays available Biome Names \n §6§lservers §r- Displays available Server Addresses \n §6§lguis §r- Displays available GUI Names \n §6§litems §r- Displays available Item Names
craftpresence.command.usage.assets=§lCraftPresence - Assets Sub-Commands: \n\n §6§llarge §r- View Discord Assets with Size LARGE \n §6§lsmall §r- View Discord Assets with Size SMALL \n §6§lall §r- View All Discord Assets
craftpresence.command.unrecognized=§c§lUnknown Command - use §6§l/craftpresence help
craftpresence.command.currentdata=§lCurrent RPC Data:§r \n §6§lDetails:§r %1$s \n §6§lGame State:§r %2$s \n §6§lStart Timestamp:§r %3$s \n §6§lClient ID:§r %4$s \n §6§lLarge Icon Key:§r %5$s \n §6§lLarge Icon Text:§r %6$s \n §6§lSmall Icon Key:§r %7$s \n §6§lSmall Icon Text:§r %8$s \n §6§lParty ID:§r %9$s \n §6§lParty Size:§r %10$s \n §6§lParty Max:§r %11$s \n §6§lJoin Secret:§r %12$s \n §6§lEnd Timestamp:§r %13$s \n §6§lMatch Secret:§r %14$s \n §6§lSpectate Secret:§r %15$s \n §6§lInstance:§r %16$s
craftpresence.command.reload=§6§lReloading CraftPresence Data, depending on Settings!
craftpresence.command.reload.complete=§2§lReloaded CraftPresence Data!
craftpresence.command.request.info=§6§lRequest Info:§r \n §6§lRequester Username: %1$s \n\n §6§lUse /cp request <accept|deny> or Wait %2$s Seconds to Ignore
craftpresence.command.request.none=§6§lYou do not have any available Join Requests!
craftpresence.command.request.accept=§6§lJoin Request Accepted! %1$s has been sent an Invite!
craftpresence.command.request.denied=§6§lJoin Request Denied from %1$s!
craftpresence.command.request.ignored=§6§lJoin Request Ignored from %1$s!
craftpresence.command.shutdown.pre=§6§lShutting Down CraftPresence...
craftpresence.command.shutdown.post=§2§lCraftPresence has been Shutdown! \n §6§lUse /cp reboot to Reboot
craftpresence.command.reboot.pre=§6§lRebooting CraftPresence...
craftpresence.command.reboot.post=§2§lCraftPresence has been Rebooted!
## LOGGING MESSAGES ##
craftpresence.logger.info.load=Loaded Display Data with ID: %1$s (Logged in as %2$s)
craftpresence.logger.info.shutdown=Shutting Down CraftPresence...
craftpresence.logger.info.config.save=Configuration Settings have been Saved and Reloaded Successfully!
craftpresence.logger.info.manifest.init=Checking for valid Curse Manifest Data...
craftpresence.logger.info.manifest.loaded=Found Curse Manifest Data! ( Name: "%1$s" )
craftpresence.logger.info.instance.init=Checking for valid MultiMC Instance Data...
craftpresence.logger.info.instance.loaded=Found MultiMC Instance Data! ( Name: "%1$s" - Icon: "%2$s" )
craftpresence.logger.info.mcupdater.init=Checking for valid MCUpdater Instance Data...
craftpresence.logger.info.mcupdater.loaded=Found MCUpdater Instance Data! ( Name: "%1$s")
craftpresence.logger.info.technic.init=Checking for valid Technic Pack Data...
craftpresence.logger.info.technic.loaded=Found Technic Pack Data! ( Name: "%1$s" - Icon: "%2$s" )
craftpresence.logger.info.chardata.init=Searching for Character and Glyph Width Data...
craftpresence.logger.info.chardata.loaded=Character and Glyph Width Data successfully Loaded!
craftpresence.logger.info.dll.init=Loading "%1$s" as a DLL...
craftpresence.logger.info.dll.loaded="%1$s" has been Successfully loaded as a DLL!
craftpresence.logger.info.download.init=Downloading "%1$s" to "%2$s"... ( From: "%3$s" )
craftpresence.logger.info.download.loaded="%1$s" has been Successfully Downloaded to "%2$s"! ( From: "%3$s")
craftpresence.logger.info.discord.assets.load=Checking Discord for Available Assets with ID: %1$s
craftpresence.logger.info.discord.assets.detected=%1$s Total Assets Detected!
craftpresence.logger.info.discord.assets.load.credits=Coded by ItsDizzy - https://github.com/ItsDizzy
craftpresence.logger.info.discord.assets.request=To Add Support for this Icon, Please Request for this Icon to be Added to the Default Client ID or Add the Icon under the following Name: "%1$s".
craftpresence.logger.info.discord.assets.fallback=Fallback Icon for "%1$s" Found! Using %2$s Icon with Name "%3$s"!
craftpresence.logger.error.system=CraftPresence was unable to retrieve System Info, Things may not work well...
craftpresence.logger.error.rpc=CraftPresence has encountered the following RPC Error (Code: %1$s), and has been Shut Down to prevent a crash: %2$s
craftpresence.logger.error.download=Failed to Download "%1$s" from "%2$s" \n Manually Download "%1$s" from "%2$s" to "%3$s"
craftpresence.logger.error.chardata=Failed to Retrieve Character & Glyph Width Data, Some Rendering Functions have been disabled...
craftpresence.logger.error.dataclose=Failed to Close a Data Stream, Resource Depletion may Occur, Please Report this Issue...
craftpresence.logger.error.dll=Failed to Load "%1$s" as a DLL, Things may not work well...
craftpresence.logger.error.delete.file=Failed to Delete "%1$s", Things may not work well...
craftpresence.logger.error.web=Failed to Go to Page: %1$s
craftpresence.logger.error.keybind=A Keybind Error has occurred, Resetting "%1$s" to default to prevent a crash...
craftpresence.logger.error.command=An Error has Occurred Executing this Command
craftpresence.logger.error.config.save=Failed to load or save Configuration
craftpresence.logger.error.config.invalidicon.empty=Unable to Detect any Usable Icons! Please Submit a Mod Issue, if this is a mistake
craftpresence.logger.error.config.invalidicon.pre=Invalid Icon "%1$s" in Property "%2$s", Selecting a Randomized Valid Icon...
craftpresence.logger.error.config.invalidicon.post=Successfully Set Icon in Property "%1$s" to "%2$s"
craftpresence.logger.error.config.emptyprop=Empty Property Detected ("%1$s"), setting property as default...
craftpresence.logger.error.config.invalidprop=Invalid Property Detected ("%1$s"), removing property...
craftpresence.logger.error.config.defaultmissing=Default Value is missing for Property "%1$s", Adding to Property...
craftpresence.logger.error.file.manifest=Unable to get Curse Manifest Data (Ignore if Not using a Twitch/CursePack)
craftpresence.logger.error.file.instance=Unable to get MultiMC Instance Data (Ignore if Not using a MultiMC Pack)
craftpresence.logger.error.file.mcupdater=Unable to get MCUpdater Instance Data (Ignore if Not using a MCUpdater Pack)
craftpresence.logger.error.file.technic=Unable to get Technic Pack Data (Ignore if Not using a Technic Pack)
craftpresence.logger.error.technic.limitation=Due to Technic Limitations, The Pack your Currently Playing will only display an Icon properly if selected in the Technic Launcher.
craftpresence.logger.error.load=Failed to Load Display Data
craftpresence.logger.error.discord.join=Join Request Rejected, due to an Invalid Join Key: %1$s
craftpresence.logger.error.discord.assets.load=Unable to get Discord Assets, Things may not work well.
craftpresence.logger.error.discord.assets.fallback=Asset Name "%1$s" does not exist, attempting to use an alternative Icon with %2$s size.
craftpresence.logger.error.discord.assets.default=Failed to Assign an Alternative Icon for Asset Name "%1$s", using Default Icon with %2$s size.
craftpresence.logger.warning.config.disabled.enablejoinrequest=Join Request Support is Disabled in Your Config. Please Enable it to accept and send Join Requests in Discord!
craftpresence.logger.warning.config.empty.clientid=Your Client ID is Empty, things may not work well...
craftpresence.logger.warning.config.conflict.biomestate=showCurrentBiome and showGameState are both enabled, giving priority to showGameState!
craftpresence.logger.warning.config.conflict.pergui=enablePerGUI and showGameState are both enabled, giving priority to showGameState!
craftpresence.logger.warning.fingerprintviolation=Invalid Certificate Detected, this Build will not receive Support!
craftpresence.logger.warning.debugmode=You are Running CraftPresence in a Debugging Environment, some features may not function properly!
